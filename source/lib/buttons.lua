buttons = {}

-- USAGE --

-- define buttons and add them to global button table

-- -- button for brake
-- local mybutton = {}
-- local buttonsequence = 1            -- sequence on the screen
-- mybutton.x = SCREEN_WIDTH - 205
-- mybutton.y = SCREEN_HEIGHT - 275
-- mybutton.width = 110               -- use this to define click zone on images
-- mybutton.height = 35
-- mybutton.bgcolour = {1,1,1,0}       -- set alpha to zero if drawing an image
-- mybutton.drawOutline = false
-- mybutton.outlineColour = {1,1,1,1}
-- mybutton.label = ""
-- mybutton.image = IMAGE[enum.imageBrakeButton]
-- mybutton.imageoffsetx = 0
-- mybutton.imageoffsety = 0
-- mybutton.imagescalex = 0.5
-- mybutton.imagescaley = 0.5
-- mybutton.labelcolour = {1,1,1,1}
-- mybutton.labeloffcolour = {1,1,1,1}
-- mybutton.labeloncolour = {1,1,1,1}
-- mybutton.labelcolour = {0,0,0,1}
-- mybutton.labelxoffset = 15
-- mybutton.state = "on"
-- mybutton.visible = true
-- mybutton.scene = enum.sceneRace               -- change and add to enum
-- mybutton.identifier = enum.buttonBrake     -- change and add to enum
-- table.insert(GUI_BUTTONS, mybutton) -- this adds the button to the global table

-- draw buttons in love.draw()
-- buttons.drawButtons()

-- process button clicks in mousereleased

-- if button == 1 then
-- 	-- see if the brake button is pressed
-- 	local clickedButtonID = buttons.getButtonID(rx, ry)
-- 	if clickedButtonID == enum.buttonBrake then
-- 		-- do something
-- 	end
-- end

GUI_BUTTONS = {}        -- global

function buttons.create(x, y, identifier)

	local mybutton = {}
	mybutton.x = x
	mybutton.y = y
	mybutton.width = 50                 -- use this to define click zone on images
	mybutton.height = 50
	mybutton.bgcolour = {1,1,1,1}       -- set alpha to zero if drawing an image
	mybutton.drawOutline = true
	mybutton.outlineColour = {1,1,1,1}
	mybutton.label = "button"
	mybutton.image = nil
	mybutton.imageoffsetx = 0
	mybutton.imageoffsety = 0
	mybutton.imagescalex = 1
	mybutton.imagescaley = 1
	mybutton.labelcolour = {1,0,0,1}
	mybutton.labeloffcolour = {1,1,1,1}
	mybutton.labeloncolour = {1,1,1,1}
	mybutton.labelxoffset = 3			-- moves to the RIGHT
	mybutton.labelyoffset = 5
	mybutton.state = "on"
	mybutton.visible = true
	mybutton.identifier = identifier     -- change and add to enum
	table.insert(GUI_BUTTONS, mybutton) -- this adds the button to the global table
	return mybutton
end

function buttons.setWidth(button, value)
	button.width = value
end

function buttons.setHeight(button, value)
	button.height = value
end

function buttons.setImage(button, image)
	button.image = image
	button.bgcolour = {1,1,1,0}		-- invisible
end

function buttons.setScaleX(button, scalex)
	button.imagescalex = scalex
end
function buttons.setScaleY(button, scaley)
	button.imagescaley = scaley
end

function buttons.setImageOffsetX(button, offset)
	button.imageoffsetx = offset
end
function buttons.setImageOffsetY(button, offset)
	button.imageoffsety = offset
end

function buttons.setDrawOutline(mybutton, value)
	-- value = true/false
	mybutton.drawOutline = value
end

function buttons.setLabel(mybutton, text)
	-- value = true/false
	mybutton.label = text
end

function buttons.setLabelColour(mybutton, colourvalues)
	-- colourvalues is a table with RGB values
	-- usage: button.setLabelColour(mybutton, {1,1,1,1} )
	mybutton.labelcolour = colourvalues
end

function buttons.setBGColour(mybutton, colourvalues)
	mybutton.bgcolour = colourvalues
end

function buttons.setVisible(mybutton, value)
	mybutton.visible = value
end

function buttons.setLabelOffsetX(mybutton, value)
	mybutton.labelxoffset = value
end

function buttons.setLabelOffsetY(mybutton, value)
	mybutton.labelyoffset = value
end

function buttons.setScene(mybutton, value)
	mybutton.scene = value
end

function buttons.getButtonObject(enumvalue)
	-- returns the button object for the given guid. Useful when needing to manipulate the button directly.
	print("HI")
	for k, button in pairs(GUI_BUTTONS) do
		if GUI_BUTTONS.identifier == enumvalue then
			return button
		end
	end
	return nil
end

-- ************************************************************

function buttons.setButtonVisible(enumvalue)
	-- receives an enum (number) and sets the visibility of that button to true
	for k, button in pairs(GUI_BUTTONS) do
		if button.identifier == enumvalue then
			button.visible = true
			break
		end
	end
end

function buttons.setButtonInvisible(enumvalue)
	-- receives an enum (number) and sets the visibility of that button to false
	for k, button in pairs(GUI_BUTTONS) do
		if button.identifier == enumvalue then
			button.visible = false
			break
		end
	end
end

function buttons.getButtonID(rx, ry)
	-- the button table is a global table
	-- check if mouse click is inside any button
	-- rx, ry = mouse click X/Y
	-- returns the identifier of the button (enum) or nil
    for k, button in pairs(GUI_BUTTONS) do
		if button.visible then
			-- print(rx, ry, button.x, button.y, button.width, button.height)
			if rx >= button.x and rx <= button.x + button.width and
				ry >= button.y and ry <= button.y + button.height then
					return button.identifier
			end
		end
	end
	-- loop finished. No result. Return nil
	return nil
end

function buttons.changeButtonLabel(enumvalue, newlabel)
	-- returns nothing
print(enumvalue)
	for k, button in pairs(GUI_BUTTONS) do
		if button.identifier == enumvalue then
			button.label = tostring(newlabel)
			break
		end
	end
end

function buttons.drawButtons()
    -- draw buttons

	local currentscene = cf.currentScreenName(SCREEN_STACK)

	for k, button in pairs(GUI_BUTTONS) do
		if button.visible and (button.scene == currentscene or button.scene == nil) then
			-- draw the button

            -- draw the bg
            love.graphics.setColor(button.bgcolour)
            love.graphics.rectangle("fill", button.x, button.y, button.width, button.height)			-- drawx/y is the top left corner of the square

            -- draw the outline
            if button.drawOutline then
                love.graphics.setColor(button.outlineColour)
                love.graphics.rectangle("line", button.x, button.y, button.width, button.height)			-- drawx/y is the top left corner of the square
            end

			-- draw the image
			if button.image ~= nil then
                love.graphics.setColor(1,1,1,1)
				love.graphics.draw(button.image, button.x, button.y, 0, button.imagescalex, button.imagescaley, button.imageoffsetx, button.imageoffsety)
			end

			-- draw the label
			local labelxoffset = button.labelxoffset or 0
			local labelyoffset = button.labelyoffset or 0
            love.graphics.setColor(button.labelcolour)
			love.graphics.print(tostring(button.label), button.x + labelxoffset, button.y + labelyoffset)
		end
	end

end

return buttons
