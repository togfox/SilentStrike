destroyer = {}

function destroyer.create()
    -- https://en.wikipedia.org/wiki/Fletcher-class_destroyer
    -- https://dosits.org/people-and-sound/history-of-underwater-acoustics/world-war-ii-1941-1945/
    -- https://dosits.org/science/advanced-topics/sonar-equation/

    local newobj = {}
    newobj.objType = enum.objTypeDestroyer

    newobj.desiredfacing = love.math.random(0, 359)

    newobj.length = 113
    newobj.width = 12
    newobj.depth = 0
    newobj.mass = 2500000      -- 2500 tons (full load)
    newobj.speed = 0
    newobj.maxforce = 12500             -- 18 m/s / 67.6 km/h
    newobj.currentforce = love.math.random(0, newobj.maxforce / 2)
    -- newobj.currentforce = newobj.maxforce
    newobj.origforce = newobj.currentforce                    -- use this to return to a normal course
    newobj.turnrate = 10
    newobj.noiseradius = 1000
    newobj.visualradius = 3000                          -- metres. Sub has chance of visual detection inside this range
    newobj.subdetectradius = 100
    newobj.imageenum = newobj.objType
    -- newobj.isdetected = false                           -- if isdetectedaudibly or isdetectedvisually then isdetected = true
    newobj.isdetectedvisually = false
    newobj.isdetectedaudibly = false
    newobj.nextdetectioncheck = 0                       -- see NEXT_DETECTION_CHECK_MASTER
    newobj.timeundetected = 0

    -- create a physical object and attach it to the table
    local thisobject = {}

    -- start x metres away from the sub using random distance and angle
    local rnddist
    if DEBUG then
        rnddist = love.math.random(100, 500)
    else
        rnddist = love.math.random(1000, newobj.visualradius)
    end
    local rndbearing = love.math.random(0, 359)
    local subx, suby = SUBMARINE[1].physobj.body:getPosition()
    x, y = cf.addVectorToPoint(subx, suby, rndbearing, rnddist)
    thisobject.body = love.physics.newBody(PHYSICSWORLD, x, y, "dynamic")
    thisobject.body:setLinearDamping(0.5)
    thisobject.body:setMass(newobj.mass)
    thisobject.body:setAngle(math.rad(love.math.random(0, 359)))    -- radians

    -- define the shape of the physical object
    local x1 = newobj.length / 2 * -1
    local y1 = newobj.width / 2 * -1
    local x2 = newobj.length / 2
    local y2 = newobj.width / 2 * -1
    local x3 = newobj.length / 2
    local y3 = newobj.width / 2
    local x4 = newobj.length / 2 * -1
    local y4 = newobj.width / 2
    thisobject.shape = love.physics.newPolygonShape(x2,y2,x3,y3,x4,y4,x1,y1)
    thisobject.fixture = love.physics.newFixture(thisobject.body, thisobject.shape, 1)		-- the 1 is the density
    thisobject.fixture:setRestitution(0.25)                                                 -- bounce factor
    thisobject.fixture:setSensor(false)                                                     -- sensors don't react physically so set this to false
    thisobject.fixture:setCategory(enum.objTypeDestroyer, enum.objTypeBattleship)

    local guid = cf.getGUID()
    thisobject.fixture:setUserData(guid)
    newobj.guid = guid

    newobj.physobj = thisobject

    newobj.alertlevel = enum.alertNormal
    newobj.alerttimer = 0                           -- use this to downgrade alert levels
    newobj.origfacing = physicslib.getCompassHeading(newobj)        -- compass value. Use this to return to a normal course
    newobj.origforce = newobj.currentforce                          -- use this to return to a normal course

    newobj.origfacing = physicslib.getCompassHeading(newobj)        -- compass value. Use this to return to a normal course
    -- newobj.imageenum = enum.imageDestroyer
    table.insert(DESTROYER, newobj)
end

function destroyer.draw(obj)
    local objx, objy = obj.physobj.body:getPosition()
    fun.drawObject(obj)

    -- draw speed for debugging reasons
    if DEBUG then
        local txt = cf.round(obj.speed, 1) .. " m/s"
        love.graphics.setColor(1,1,1,1)
        love.graphics.print(txt, objx + 15, objy - 5)
    end
end

function destroyer.update(dt)

    for k, dest in pairs(DESTROYER) do
        -- local obj = fun.targetHit(dest)          -- returns nil or the object that is struck
        -- if obj ~= nil then
        --     -- see if hit submarine
        --     if obj.objType == enum.objTypeSubmarine then
        --         if TIMER_SUB_DEAD <= 0 then
        --             -- lovelyToasts.show("You are dead!")
        --             TIMER_SUB_DEAD = 15             -- stops the toast appearing too often
        --         end
        --     end
        -- end

        -- fun.detectObjects(dest, dt)        -- see if anything is detected and update the alert status. Also downgrades alert status

        fun.applyHelm(dest, dt)
        fun.move(dest, dt)
    end
end

return destroyer
