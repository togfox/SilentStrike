sonar = {}

local SCOPELINEHEADING = 0      -- degrees

local function determineScopeStatic(x, y, radius)
    -- create static blocks originating from x/y out to a distance of radius
    -- the blocks will centred randomly (not top left corner). This may mean corner of blocks exceed the radius
    -- will create a random alpha value for more randomness

    assert(x ~= nil)

    local numofstatic = STATIC_DENSITY

    STATIC = {}

    for i = 1, numofstatic do

        local rnddist = radius * math.sqrt(love.math.random())
        local rndheading = love.math.random() * 2 * math.pi * radius

        local block = {}
        block.x, block.y = cf.addVectorToPoint(x, y, rndheading, rnddist)
        block.alpha = love.math.random(10,40) / 100                            -- the alpha value will eventually rise and fall based on environment noise
        block.colour = love.math.random(1, 10) / 10                             -- creates different scales of grey
        table.insert(STATIC, block)
    end
end

local function drawStatic()
    for k, block in pairs(STATIC) do
        -- block.x and block.y is the centre. Need to determine top left corner
        local drawx = block.x - (STATICWIDTH / 2)
        local drawy = block.y - (STATICHEIGHT / 2)
        -- draw static
        love.graphics.setColor(0, block.colour, 0, block.alpha)        -- all this is determined in determineScopeStatic function
        love.graphics.rectangle("fill", drawx, drawy, STATICWIDTH, STATICHEIGHT)
    end
    love.graphics.setColor(1,1,1,1)
end

local function updateMarks(dt)
    for i = #SONAR_MARKS, 1, -1 do
        SONAR_MARKS[i].timeleft = SONAR_MARKS[i].timeleft - dt            -- fade the line over time
        if SONAR_MARKS[i].timeleft <= 0 then table.remove(SONAR_MARKS, i) end
    end
end

function sonar.mousereleased(x, y, button, isTouch)
    -- called from main.mousereleased

    local rx, ry = res.toGame(x,y)
    local camx, camy = cam:toWorld(x, y)	-- converts screen x/y to world x/y

    if button == 1 then

        -- need to translate the mouse click to navigator co-ordinates

        -- get bearing and distance w/r to the bow of the sub (facing is irrelevant)
        local subx = SCREEN_WIDTH  / 2          -- this is the centre of the scope and not the real subx/y
        local suby = SCREEN_HEIGHT / 2
        local bearing = cf.getBearing(subx, suby, rx, ry)
        local dist = cf.getDistance(subx, suby, rx, ry)           -- this is raw pixels and not any real distance.

        -- translate bearing and distance to x/y for navigator screen
        local subx, suby = SUBMARINE[1].physobj.body:getPosition()
        local subfacing = physicslib.getCompassHeading(SUBMARINE[1])
        bearing = cf.adjustHeading(bearing, subfacing)          -- add the sub facing to the bearing
        dist = dist * (SCOPE_RANGE / SCOPE_RADIUS)

        -- find a point with bearing and distance accounting for facing
        local x, y = cf.addVectorToPoint(subx, suby, bearing, dist)

        local newmark = {}
        newmark.x = x
        newmark.y = y
        newmark.timeleft = LINE_TIME
        table.insert(SONAR_MARKS, newmark)
    end
end

local function drawSonarTarget(obj)
    -- a sub-function of sonar.draw.
    -- obj is a destroyer, battleship or sonar mark
    local drawx = SCREEN_WIDTH  / 2
    local drawy = SCREEN_HEIGHT / 2
    local subx, suby = SUBMARINE[1].physobj.body:getPosition()

    -- this needs to handle objects with physical bodies as well as marks that have no physical bodies
    local x2, y2
    if obj.physobj ~= nil then
        x2, y2 = obj.physobj.body:getPosition()
    else
        x2 = obj.x          -- these are set on the sonar screen during mouse click
        y2 = obj.y
    end

    local dist = cf.getDistance(subx, suby, x2, y2)
    if dist <= SCOPE_RANGE then            --! this is the assumed range of the sonar
        -- draw blip
        dist = dist / SCOPE_RANGE * SCOPE_RADIUS       -- positions blip proportional to edge of sonar
        local bearing = cf.getBearing(subx, suby, x2, y2)
        bearing = bearing - math.deg(SUBMARINE[1].physobj.body:getAngle()) - 90
        if bearing < 0 then bearing = 360 + bearing end
        local x3, y3 = cf.addVectorToPoint(drawx, drawy, bearing, dist)

        -- sonar marks are drawn different to confirmed contacts
        love.graphics.setColor(0,1,0,1)
        if obj.physobj ~= nil then
            love.graphics.circle("fill", x3, y3, 5)
        else
            local squaresize = 40
            x3 = x3 - (squaresize / 4)
            y3 = y3 - (squaresize / 4)
            love.graphics.setColor(1,0,0, obj.timeleft / LINE_TIME)
            love.graphics.rectangle("line", x3, y3, squaresize / 2, squaresize / 2)

        end
    end
    love.graphics.setColor(1,1,1,1)
end

function sonar.draw()

    love.graphics.setBackgroundColor(0,0,0, 1 )
    -- draw the scope circle
    local drawx = SCREEN_WIDTH  / 2
    local drawy = SCREEN_HEIGHT / 2
    love.graphics.setColor(1,1,1,1)
    love.graphics.circle("line", drawx, drawy, SCOPE_RADIUS)
    drawStatic()

    -- draw the submarine
    -- determine offset based on the image file
    local obj = SUBMARINE[1]
    local xoffset = IMAGE[obj.imageenum]:getWidth() / 2
    local yoffset = IMAGE[obj.imageenum]:getHeight() / 2

    local xscale = obj.width / IMAGE[obj.imageenum]:getWidth()
    local yscale = obj.length / IMAGE[obj.imageenum]:getHeight()

    -- draw the image
    love.graphics.setColor(1,1,1,1)
    local facingrad = math.rad(0)
    love.graphics.draw(IMAGE[obj.imageenum], drawx, drawy, facingrad, xscale, yscale, xoffset, yoffset)

    -- draw the targets
    for k, obj in pairs(BATTLESHIP) do
        if obj.isdetectedaudibly then
            drawSonarTarget(obj)
        end
    end
    for k, obj in pairs(DESTROYER) do
        if obj.isdetectedaudibly then
            drawSonarTarget(obj)
        end
    end
    for k, mark in pairs(SONAR_MARKS) do
        drawSonarTarget(mark)
    end

    -- draw sonar line
    love.graphics.setColor(0,1,0,1)
    local x2, y2 = cf.addVectorToPoint(drawx, drawy, SCOPELINEHEADING, SCOPE_RADIUS)
    love.graphics.line(drawx, drawy, x2, y2)
    love.graphics.setColor(1,1,1,1)

end

function sonar.update(dt)
    local x, y = SUBMARINE[1].physobj.body:getPosition()

    STATIC_TIMER = STATIC_TIMER - dt
    if STATIC_TIMER <= 0 then
        determineScopeStatic(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, SCOPE_RADIUS)            -- x/y is the sub position (centre of screen).
        STATIC_TIMER = STATIC_TIMER_MASTER
    end

    -- update the scope line
    SCOPELINEHEADING = SCOPELINEHEADING + (dt * 45)      -- arbitary value to make line move faster
    if SCOPELINEHEADING > 360 then SCOPELINEHEADING = SCOPELINEHEADING - 360 end

    -- make the marks fade out
    updateMarks(dt)
end

return sonar
