physics = {}

function physics.getCompassHeading(obj)
    -- takes the physical facing and returns the compass heading in degrees. It does this by converting to degrees then adding 90 degrees
    -- obj is NOT a phyics object - it is a table object
    -- returns degrees
    -- does not round
    local physobj = obj.physobj
    local currentfacing = math.deg(physobj.body:getAngle())         -- degrees
    return fun.getCompassValue(currentfacing)
end

function physics.validateAngle(obj)
    -- ensure angle is within 0 -> 2 PI
    -- obj is NOT a phyics object - it is a table object
    local physobj = obj.physobj
    while physobj.body:getAngle() > (math.pi * 2) do
        physobj.body:setAngle(physobj.body:getAngle() - (math.pi * 2))
    end
    while physobj.body:getAngle() < (math.pi * - 2) do
        physobj.body:setAngle(physobj.body:getAngle() + (math.pi * 2))
    end
end

function physics.turnToFacing(obj, dt)
    -- obj is not a physics object
    -- math.pi / 2 = 90 degrees

    local currentfacing = cf.round(physicslib.getCompassHeading(obj), 4)     -- compass
    local desiredfacing = tonumber(obj.desiredfacing)                     -- compass

    -- print(currentfacing, desiredfacing)

    local turndirection = cf.getTurningAdjustment(currentfacing, desiredfacing)     -- can be -1 through to +1

    local newcurrentfacing = currentfacing + (turndirection * dt * obj.turnrate)
    local newcurrentfacingrad = math.rad(newcurrentfacing)
    newcurrentfacingrad = newcurrentfacingrad - (math.pi / 2)
    obj.physobj.body:setAngle(newcurrentfacingrad)

    -- if DEBUG and obj.objType == enum.objTypeSubmarine then
    --     print(currentfacing, desiredfacing, turndirection, newcurrentfacing, newcurrentfacingrad)
    -- end

end

return physics
