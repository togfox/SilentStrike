# Silent strike

## ![Click here for Releases](<https://codeberg.org/togfox/SilentStrike/releases>)

Try to sink the ships that are around you. Your submarine controls are on the top left.

![](<https://i.postimg.cc/Gh3x09z2/image.png>)

Aim torpedoes by typing a compass heading next to each tube then press the red FIRE button.

![](<https://i.postimg.cc/wTLDJg1H/image.png>)

Check the tube status lights and press the fire button to launch a torpedo. Don't forget to reload them!

![](<https://i.postimg.cc/PqQMshWT/image.png>)

Change depth by typing in a new depth. Drive on the surface of the water to go faster.

![](<https://i.postimg.cc/WzrqMwbS/image.png>)

Draw lines on your 'map' with the left mouse button (ruler). The lines will automatically fade over time.

![](<https://i.postimg.cc/FKy9gCjY/image.png>)

Use the stop watch and ruler to calculate the speed of each object.

![](<https://i.postimg.cc/2jbC05Fp/image.png>)

Adjust game time to skip the boring bits and press SPACE BAR to pause.

![](<https://i.postimg.cc/KcHRYVQb/image.png>)

### Controls

keypad 5 = centre camera on your submarine

keypad + = speed up game time

keypad - = slow down game time

control-z to undo the last line/dot drawn

spacebar = pause

F1 = navigators station (view the top-down ocean)

F2 = sonar station (like radar - but sonar)

left mouse button = draw lines on 'map' like a ruler (drag left mouse button)

middle mouse button = pan the camera

right mouse button = pan the camera

mouse scroll = zoom in/out


edit numbers on top bar to change direction

edit numbers on each tube to aim torpedoes

edit numbers on each tube to change your depth

### **Installation**

#### <ins>First time (Windows)</ins>

Download the zip file

Unzip zip file

Run exe file inside game folder


#### <ins>Upgrade a previous install (Windows)</ins>

Download exe file

Move exe file into your game folder (downloaded previously)

Run exe file


#### <ins>Love developers</ins>

Download Love file

Run Love file

# Aim of the game

Setting up a perfect target solution against an unsuspecting prey is challenging and satisfying. The below steps are most accurate if you are in front of your intended target.

## Detect your target

The first challenge is to find a target. If you can't see any targets on the screen then zoom out with the mouse wheel then pan around (middle mouse button) until you find a target. If none are in view then you have some options:
- set speed to 100% (full flank) and start moving in a random direction
- make sure your depth is somewhere between 0 (surface) and 15 metres (periscope depth)
- restart the session and hope you have better luck.

Submerging to deeper than periscope depth means you can't see any targets with your periscope but you can still hear them. Press F2 to switch to sonar view. You can see your sonar updating every few seconds. You can hear targets at any depth under the following conditions:
- the target is making noise
- the target is not too far way
- your sub is not making too much noise.

The maximum range of your sonar is 2000 metres. Also note your sonar shows targets relative to your bow (front). A target that appears in front of you on the sonar might actually be west of your position if you are facing west.

If you can see a target on the sonar, click it with your mouse to "mark" it. This is a static mark. It won't move with the target, but you can now move to your main navigator window (F1) and look closely to see where your sonar mark is. You can use these marks to track targets between screens and stations.

Once you find a target on the sonar or in your navigator screen then try to determine a firing solution.

## Step 1 - determine the direction of the target

Use the ruler to draw a line through the target from stern to bow. The ruler will show the direction of travel. this is the track line. Use CONTROL-Z to remove the line if you make a mistake.

![](<https://i.postimg.cc/SKLTYHK4/image.png>)

## Step 2 - determine the speed of the target

Use the left-mouse button to put a dot on the bow of the target then quickly start the stopwatch. After a period of time (I suggest at least 10 seconds) put a 2nd dot on the bow of the target. Now use the ruler to measure the distance between the two dots and then divide by the number of seconds on the stop watch.

example: two dots are 70 metres apart after 10 seconds = 7 metres per seconds

![](<https://i.postimg.cc/76hSv1VF/image.png>)

## Step 4 - determine the point of impact

The greatest chance to hit is with a direct broadside. This means 90 degrees from the direction of travel. Draw a line from your submarine to the target's track line so that the torpedo's track is 90 degrees to the target's track.

![](<https://i.postimg.cc/50HdXGgn/image.png>)

## Step 5 - know how fast your torpedos travel

You can remember your own torpedoes travel about 14 m/s. Sometimes more, sometimes less. Nature is never precise.

## Step 6 - determine how long it will take to hit the target

Divide the distance from your submarine to the target by ~14 m/s. This is how much travel time your torpedo will use.

example: 900 metres to target / ~14 m/s = 64 seconds of travel time

## Step 7 - determine firing point

Determine how fast your target will move while the torpedo is travelling, then measure the distance backwards from that interception point.

example: target speed = 7 m/s * 64 seconds for torpedo travel time = 448 metres. Measure 448 metres backwards from the interception point.

![](<https://i.postimg.cc/d3PHqt0m/image.png>)

## Step 8 - fire torpedo!

Ensure the gyro is set on your torpedo and wait for the target to reach the launch point. If you launch your torpoedo on the right angle and right time then you will hit your target.  :)

![](<https://i.postimg.cc/yYJxq1Zr/image.png>)

## Step 9 - firing solution success!

![](<https://i.postimg.cc/qqgDKNg1/image.png>)















